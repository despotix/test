var a1 = [
    {_id:1, someKey: "RINGING", meta: { subKey1: 1234, subKey2: 52 } }
];
var a2 = [
    {_id:1, someKey: "HANGUP", meta: { subKey1: 1234 } },
    {_id:2, someKey: "RINGING", meta: { subKey1: 5678, subKey2: 207, subKey3: 52 } }
];

var html = require('./diff.js').arrayDiffToHtmlTable(a1, a2);
console.log(html);
require('fs').writeFileSync('./results/1.html', html);