var ECT = require('ect');

function linearize(o, parent_key) {
    if(!o) {
        return null;
    }
    var rslt = {};
    Object.keys(o).forEach(function(key){
        if('object' == typeof o[key]) {
            var tmp = linearize(o[key], key);
            Object.keys(tmp).forEach(function(k){
                rslt[parent_key?parent_key+'_'+k:k] = tmp[k];
            });
        } else {
            rslt[parent_key?(parent_key+'_'+key):key] = o[key];
        }
    });
    return rslt;
}

function arrayDiffToJSON( prevArray, currArray) {
    var o = {};

    prevArray.forEach(function(p){
        o[p._id] = [ linearize(p) ];
    });
    currArray.forEach(function(c){
        if(!o[c._id]){
            o[c._id] = [ null ];
        }
        o[c._id].push(linearize(c));
    });

    var all_fields = {};
    // filling all fields superset
    Object.keys(o).forEach(function(id){
        var o1 = o[id][0];
        var o2 = o[id][1];
        Object.keys(o1||{}).forEach(function(k){
            all_fields[k] = true;
        });
        Object.keys(o2||{}).forEach(function(k){
            all_fields[k] = true;
        });
    });

    var diff = {};
    Object.keys(o).forEach(function(id){
        diff[id] = {};
        var o1 = o[id][0]||{};
        var o2 = o[id][1]||{};
        Object.keys(all_fields).forEach(function(field){
            var v1 = o1[field];
            var v2 = o2[field];
            var color = 'white'; // not changed
            if(!v1&&v2){
                color = 'green'; //inserted
            } else if(v1&&!v2){
                color = 'red'; //deleted
            } else if(v1!=v2){
                color = 'blue'; // changed
            }
            diff[id][field] = {
                color : color,
                value : v2||''
            }
        });
    });
    return diff;
}


module.exports.arrayDiffToHtmlTable = function( prevArray, currArray ) {
    var html = [
        '<table border="1">',
        '<thead>',
        '<tr>',
        '<% for id of @result : %>',
        '<% for field of @result[id] : %>',
        '<td><%-field%></td>',
        '<% end %>',
        '<% break %>',
        '<% end %>',
        '</tr>',
        '</thead>',
        '<tbody>',
        '<% for id of @result : %>',
        '<tr>',
        '<% for field of @result[id] : %>',
        '<td bgcolor="<%-@result[id][field].color%>"><%-@result[id][field].value%></td>',
        '<% end %>',
        '</tr>',
        '<% end %>',
        '</tbody>',
        '</table>'
    ].join('\n');
    return ECT({root:{page:html}}).render('page', {
        result : arrayDiffToJSON(prevArray, currArray)
    });
};